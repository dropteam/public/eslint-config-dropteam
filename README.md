# Dropteam ESLint config

> A sharable ESLint config for Dropteam's projects

## Install

```
yarn add --dev eslint-config-dropteam
```

## Usage
Extend your ESLint [configuration](https://eslint.org/docs/user-guide/configuring) with this config. You can still overrides thoses rules in your own project.

```
{
  "extends": ["eslint-config-dropteam"] 
}
```

## Documentation

This configuration is currently based on thoses configurations and plugins :

* [eslint-config-prettier](https://github.com/prettier/eslint-config-prettier)
* [eslint-plugin-node](https://github.com/mysticatea/eslint-plugin-node)
* [eslint-plugin-import](https://github.com/benmosher/eslint-plugin-import)
* [eslint-plugin-promise](https://github.com/xjamundx/eslint-plugin-promise)
* [eslint-plugin-standard](https://github.com/standard/eslint-plugin-standard#readme)

### Rules

* [prefer-arrow-callback](https://eslint.org/docs/rules/prefer-arrow-callback) - This rule locates function expressions used as callbacks or function arguments. An error will be produced for any that could be replaced by an arrow function without changing the result.

## Tests

You can write and tests rules with [eslint-config-rule-tester](https://www.npmjs.com/package/eslint-config-rule-tester), check tests in [Jessie config](https://github.com/Agoric/eslint-config-jessie) or files in `test/rules` for more informations.