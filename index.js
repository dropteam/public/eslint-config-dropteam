'use strict'

module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    jest: true,
    node: true
  },
  extends: ['plugin:prettier/recommended'],
  plugins: ['import', 'node', 'promise', 'standard'],
  rules: {
    'prefer-arrow-callback': ['error']
  }
}
