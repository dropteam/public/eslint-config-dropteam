const configTester = require('eslint-config-rule-tester')
const path = require('path')

const dropteamConfig = require('../index')

console.log(dropteamConfig)

// iterate through all the tests
Object.keys(dropteamConfig.rules).forEach(rule => {
  // eslint-disable-next-line global-require
  const test = require(path.resolve(__dirname, './rules/', rule)) // eslint-disable-line import/no-dynamic-require
  configTester(rule, dropteamConfig, test)
})
