module.exports = {
  valid: [
    `foo(a => a)
`
  ],
  invalid: [
    {
      code: `foo(function(a) {
  return a
})
`,
      errors: ['Unexpected function expression.']
    }
  ]
}
